package de.pernpas.eclipse.plugin.commons;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.ui.JavaUI;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.handlers.HandlerUtil;

public class EditorHelper {
	public static ICompilationUnit getActiveEditorInput(ExecutionEvent event) {
		IEditorInput editorInput = HandlerUtil.getActiveEditor(event).getEditorInput();
		IJavaElement javaElement = JavaUI.getEditorInputJavaElement(editorInput);
		if (javaElement instanceof ICompilationUnit) {
			return (ICompilationUnit) javaElement;
		} else {
			Reject.reject("Please select a java file");
		}
		return null;
	}
}
