package de.pernpas.eclipse.plugin.commons;



public class Reject {
	public static void reject(String message) {
		throw new BuilderException(message);
	}

	public static void ifTrue(boolean toCheck, String message) {
		if (toCheck) {
			throw new BuilderException(message);
		}
	}

	public static void reject(Exception e) {
		throw new BuilderException("Error while create the builder: " + e.getMessage());
	}
}
