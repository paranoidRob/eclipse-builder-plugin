package de.pernpas.eclipse.plugin.commons.analyze;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.jdt.core.IField;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;

import de.pernpas.eclipse.plugin.builder.BuilderImplementer;
import de.pernpas.eclipse.plugin.commons.Reject;

public class BeanHelper {

	public boolean isNoConstant(IField beanField) {
		String fieldName = beanField.getElementName();
		return !StringUtils.isAllUpperCase(StringUtils.remove(fieldName, "_"));
	}

	public boolean hasNoBeanField(IType builder) {
		IField beanField = builder.getField(BuilderImplementer.BEAN_FIELD_NAME);
		return !beanField.exists();
	}

	public boolean isMethodExisting(IType builder, String methodName) {
		try {
			IMethod[] methods = builder.getMethods();
			for (IMethod method : methods) {
				if (method.getElementName().equals(methodName)) {
					return true;
				}
			}
		} catch (JavaModelException e) {
			Reject.reject(e);
		}
		return false;
	}

	public boolean isNotSerialVersionUID(IField beanField) {
		return !beanField.getElementName().equals("serialVersionUID");
	}

}
