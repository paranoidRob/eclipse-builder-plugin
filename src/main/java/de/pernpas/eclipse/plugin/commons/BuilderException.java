package de.pernpas.eclipse.plugin.commons;

public class BuilderException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public BuilderException(String message) {
		super(message);
	}

}
