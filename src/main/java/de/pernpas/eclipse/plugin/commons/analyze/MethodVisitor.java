package de.pernpas.eclipse.plugin.commons.analyze;

import java.util.List;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.IExtendedModifier;
import org.eclipse.jdt.core.dom.MethodDeclaration;

public class MethodVisitor extends ASTVisitor {

	private boolean hasPrivateDefaultConstructor = false;
	private boolean hasPublicDefaultConstructor = false;

	@SuppressWarnings("unchecked")
	@Override
	public boolean visit(MethodDeclaration node) {
		if (node.isConstructor()) {
			List<IExtendedModifier> modifiers = node.modifiers();
			for (IExtendedModifier modifier : modifiers) {
				if (modifier.toString().contains("private")) {
					this.hasPrivateDefaultConstructor = true;
				}
				if (modifier.toString().contains("public")) {
					this.hasPublicDefaultConstructor = true;
				}
			}
		}
		return super.visit(node);
	}

	public boolean hasPrivateDefaultConstructor() {
		return hasPrivateDefaultConstructor;
	}

	public boolean hasPublicDefaultConstructor() {
		return hasPublicDefaultConstructor;
	}

}
