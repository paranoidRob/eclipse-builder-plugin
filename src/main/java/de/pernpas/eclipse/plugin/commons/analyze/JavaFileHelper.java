package de.pernpas.eclipse.plugin.commons.analyze;

import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;

import com.google.common.base.Optional;

import de.pernpas.eclipse.plugin.builder.BuilderCreator;
import de.pernpas.eclipse.plugin.commons.Reject;

public class JavaFileHelper {

	public Optional<IType> findBuilderType(ICompilationUnit javaFile, String beanName) {
		try {
			IType[] allTypes = javaFile.getAllTypes();
			for (IType type : allTypes) {
				if (type.getElementName().equals(beanName + BuilderCreator.BUILDER_CLASS_SUFFIX)) {
					return Optional.of(type);
				}
			}
		} catch (JavaModelException e) {
			Reject.reject(e);
		}
		return Optional.absent();
	}

	public IType getBeanType(ICompilationUnit javaElement) {
		try {
			IType[] types = javaElement.getAllTypes();
			return types[0];
		} catch (Exception e) {
			Reject.reject(e);
		}
		return null;
	}
}
