package de.pernpas.eclipse.plugin.builder.handler;

import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.handlers.HandlerUtil;

import com.google.common.base.Joiner;

import de.pernpas.eclipse.plugin.builder.BuilderCreator;
import de.pernpas.eclipse.plugin.commons.Constants;
import de.pernpas.eclipse.plugin.commons.EditorHelper;

public class BuilderHandler extends AbstractHandler {

	private Shell shell;

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		shell = HandlerUtil.getActiveWorkbenchWindow(event).getShell();
		try {
			createBuilder(EditorHelper.getActiveEditorInput(event));
		} catch (Exception e) {
			e.printStackTrace();
			MessageDialog.openWarning(shell, Constants.DIALOG_TITLE, "Error creating builder");
		}
		return null;
	}

	private void createBuilder(ICompilationUnit javaElement) throws JavaModelException {
		BuilderCreator builderCreator = new BuilderCreator(javaElement);
		builderCreator.createBuilder();
		if (builderCreator.hasWarnings()) {
			MessageDialog.openWarning(shell, Constants.DIALOG_TITLE, format(builderCreator.getWarnings()));
		} else {
			MessageDialog.openInformation(shell, Constants.DIALOG_TITLE, Constants.SUCCESFULLY_MESSAGE);
		}
	}

	private String format(List<String> warnings) {
		return Joiner.on("\n").join(warnings);
	}

}
