package de.pernpas.eclipse.plugin.builder.handler;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IType;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.handlers.HandlerUtil;

import com.google.common.base.Optional;

import de.pernpas.eclipse.plugin.builder.BuilderImplementer;
import de.pernpas.eclipse.plugin.commons.Constants;
import de.pernpas.eclipse.plugin.commons.EditorHelper;
import de.pernpas.eclipse.plugin.commons.Reject;
import de.pernpas.eclipse.plugin.commons.analyze.JavaFileHelper;

public class WithMethodHandler extends AbstractHandler {

	private Shell shell;
	private JavaFileHelper javaFileHelper = new JavaFileHelper();

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		shell = HandlerUtil.getActiveWorkbenchWindow(event).getShell();
		try {
			ICompilationUnit compilationUnit = EditorHelper.getActiveEditorInput(event);
			IType bean = javaFileHelper.getBeanType(compilationUnit);

			Optional<IType> builder = javaFileHelper.findBuilderType(compilationUnit, bean.getElementName());
			if (builder.isPresent()) {
				int numberOfCreatedWithMethods = new BuilderImplementer(builder.get(), bean).createWithMethods();
				MessageDialog.openInformation(shell, Constants.DIALOG_TITLE, String.format("Number of created with methods: %d", numberOfCreatedWithMethods));
			} else {
				Reject.reject("Builder class does not exist. Can not create with methods");
			}
		} catch (Exception e) {
			e.printStackTrace();
			MessageDialog.openWarning(shell, Constants.DIALOG_TITLE, "Error creating builder");
		}
		return null;
	}
}
