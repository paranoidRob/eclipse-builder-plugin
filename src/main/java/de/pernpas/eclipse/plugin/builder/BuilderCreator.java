package de.pernpas.eclipse.plugin.builder;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.CompilationUnit;

import com.google.common.base.Joiner;
import com.google.common.base.Optional;

import de.pernpas.eclipse.plugin.commons.analyze.AstParser;
import de.pernpas.eclipse.plugin.commons.analyze.JavaFileHelper;
import de.pernpas.eclipse.plugin.commons.analyze.MethodVisitor;

public class BuilderCreator {

	public static final String BUILDER_CLASS_SUFFIX = "Builder";
	private ICompilationUnit javaFile;
	private CompilationUnit astCompilationUnit;
	private List<String> warnings = new ArrayList<>();
	private JavaFileHelper javaFileHelper = new JavaFileHelper();

	public BuilderCreator(ICompilationUnit javaFile) {
		this.javaFile = javaFile;
		this.astCompilationUnit = new AstParser().parse(javaFile);
	}

	public void createBuilder() throws JavaModelException {
		System.out.println("Creating builder ...");

		IType bean = javaFileHelper.getBeanType(javaFile);
		createPrivateDefaultConstructor(bean);

		Optional<IType> optionalBuilderType = javaFileHelper.findBuilderType(javaFile, bean.getElementName());
		IType builder = optionalBuilderType.isPresent() ? optionalBuilderType.get() : createNewBuilder(bean);

		new BuilderImplementer(builder, bean).implement();
	}

	private IType createNewBuilder(IType bean) throws JavaModelException {
		return bean.createType(Joiner.on("").join("\n\npublic static class ", builderName(bean), "{\n}"), null, false, null);
	}

	private void createPrivateDefaultConstructor(IType bean) throws JavaModelException {
		MethodVisitor methodVisitor = new MethodVisitor();
		astCompilationUnit.accept(methodVisitor);
		if (!methodVisitor.hasPrivateDefaultConstructor() && !methodVisitor.hasPublicDefaultConstructor()) {
			bean.createMethod(getPrivateConstructor(bean), null, false, null);
		} else if (methodVisitor.hasPublicDefaultConstructor()) {
			warnings.add("Please change the modifier of your default constructor to \"private\"");
		}
	}

	private String getPrivateConstructor(IType bean) {
		return Joiner.on("").join("private ", bean.getTypeQualifiedName(), "(){\n}");
	}

	private String builderName(IType beanType) {
		return Joiner.on("").join(beanType.getElementName(), BUILDER_CLASS_SUFFIX);
	}

	public List<String> getWarnings() {
		return warnings;
	}

	public boolean hasWarnings() {
		return warnings.size() > 0;
	}
}
