package de.pernpas.eclipse.plugin.builder;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.eclipse.jdt.core.IField;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;

import com.google.common.base.Joiner;

import de.pernpas.eclipse.plugin.commons.Reject;
import de.pernpas.eclipse.plugin.commons.analyze.BeanHelper;

public class BuilderImplementer {

	private static final String BUILD_METHOD_NAME = "build";
	private static final String VALIDATE_METHOD_NAME = "validate";
	public static final String WITH_METHOD_PREFIX = "with";
	public static final String BEAN_FIELD_NAME = "bean";
	private IType builder;
	private IType bean;
	private BeanHelper beanHelper;

	public BuilderImplementer(IType builder, IType beanType) {
		this.builder = builder;
		this.bean = beanType;
		this.beanHelper = new BeanHelper();
	}

	public void implement() {
		try {

			createBeanField();
			createWithMethods();
			createValidateMethod();
			createBuildMethods();

		} catch (Exception e) {
			Reject.reject(e);
		}

	}

	private void createBeanField() throws JavaModelException {
		if (beanHelper.hasNoBeanField(builder)) {
			builder.createField(getBeanField(), null, false, null);
		}
	}

	private void createValidateMethod() throws JavaModelException {
		if (!beanHelper.isMethodExisting(builder, VALIDATE_METHOD_NAME)) {
			builder.createMethod("private void " + VALIDATE_METHOD_NAME + "(){\n}", null, false, null);
		}
	}

	private void createBuildMethods() throws JavaModelException {
		String buildImplementation = Joiner.on("").join("\tvalidate();\t", bean.getTypeQualifiedName(), " myBean = bean;\n\tbean=null;\n\t", "return myBean;");
		String buildMethod = Joiner.on("").join("public ", bean.getTypeQualifiedName(), " ", BUILD_METHOD_NAME, "()\n{", buildImplementation, "\n}");

		if (!beanHelper.isMethodExisting(builder, BUILD_METHOD_NAME)) {
			builder.createMethod(buildMethod, null, false, null);
		}
	}

	public int createWithMethods() throws JavaModelException {
		int numberOfCreatedWithMethods = 0;
		for (IField beanField : bean.getFields()) {
			if (beanHelper.isNoConstant(beanField) && beanHelper.isNotSerialVersionUID(beanField) && !beanHelper.isMethodExisting(builder, getWithMethodName(beanField))) {
				builder.createMethod(getMethod(beanField), null, false, null);
				numberOfCreatedWithMethods++;
			}
		}
		return numberOfCreatedWithMethods;
	}

	private String getMethod(IField beanField) throws JavaModelException {
		String parameterType = getType(beanField.getTypeSignature());
		String parameterName = beanField.getElementName();
		return Joiner.on("")
				.join("public ", builder.getElementName(), " ", getWithMethodName(beanField), "(", parameterType, " ", parameterName, "){\n", getMethodImplementation(parameterName), "\n}");
	}

	private String getMethodImplementation(String parameterName) {
		return Joiner.on("").join("\tbean.", parameterName, " = ", parameterName, ";\n\treturn this;");
	}

	private String getType(String typeSignature) {
		typeSignature = StringUtils.removeEnd(typeSignature, ";");
		typeSignature = StringUtils.removeStart(typeSignature, "Q");

		typeSignature = StringUtils.replace(typeSignature, ";>", ">");
		typeSignature = StringUtils.replace(typeSignature, "<Q", "<");

		typeSignature = handlePrimitiveTypeSignatures(typeSignature);

		return typeSignature;
	}

	private String handlePrimitiveTypeSignatures(String typeSignature) {

		switch (typeSignature) {
		case "I":
			return "int";
		case "B":
			return "byte";
		case "S":
			return "short";
		case "J":
			return "long";
		case "F":
			return "float";
		case "D":
			return "double";
		case "Z":
			return "boolean";
		case "C":
			return "char";
		default:
			return typeSignature;
		}
	}

	private String getBeanField() {
		return Joiner.on(" ").join("private", bean.getTypeQualifiedName(), BEAN_FIELD_NAME + ";");
	}

	private String getWithMethodName(IField beanField) {
		return Joiner.on("").join(WITH_METHOD_PREFIX, WordUtils.capitalize(beanField.getElementName()));
	}
}
